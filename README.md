### Analysis plan: Predicting transition to depression from UK Biobank data

*Authors: Herman Galioulline, Sam Harrison, Ines Borges Pereira, Yu Yao, Stefan Frässle, Jakob Heinzle, Klaas Enno Stephan*

This repository contains the analysis plan for projects that use fMRI data of the UK Biobank to predict transition to depression.

The main analysis plan outlines the general idea and briefly describes subprojects that all follow the same general strategy.

Subprojects that are built on this master analysis plan are placed in subfolders of this repository. The analysis plans of subprojects are more detailed and outline the methods used after they have been optimized on the training data set. Crucially, before a subproject is allowed to be tested on the test data set, the detailed analysis plan needs to be uploaded.

Current state of subprojects:

- subproject_MSc_FP contains the analysis plan for project number 2 of the main plan: **Predicting Depression from Small-Network Effective Connectivity**
- subproject_MSc_HG contains the analysis plan for project number 3 of the main plan: **Comparing the Prediction of Depression with Different Versions of DCM for Resting-State fMRI**
